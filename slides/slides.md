---
title: Fuse and Docker
hideFirstSlide: true
authors: José Moreira
email: "jose.moreira@container-solutions.com"
---

# Bio

* Software Engineer @ Container Solutions
* Full-stack developer but mostly backend
* Passionate about architecture and infrastructure
* ❤ containers

---

**Agenda**

* Intro
* Use-case
* What is Fuse?
* Useful Fuse implementations
* Utilising Fuse with containers
* Security considerations
* Docker/Kubernetes Demo
* Questions?

---
**Caveat Emptor**

Beware the noob :-)

---
**Use case architecture**

* Event driven micro-service architecture (RabbitMQ)
* Useful for developers to be able to record and replay events
* Record-and-replay tool reads and writes to JSON files
* Project currently hosted on Google Cloud (Google Kubernetes Engine)

---
**Use case Requirements**

* Allow container process to read and write from persistent storage
* Storage should easily be accesible by team members
* Object storage is more accessible for humans
* Portable across cloud providers
* Use case container is a development helper, not a mission critical tool

---
**What is Fuse (Filesystem in Userspace)**

"[...] software interface for Unix-like computer operating systems that lets non-privileged users create their own file systems without editing kernel code."

Source: https://en.wikipedia.org/wiki/Filesystem_in_Userspace

---
**Useful Fuse implementations**

* gcsfuse (https://github.com/GoogleCloudPlatform/gcsfuse): A user-space file system for interacting with Google Cloud Storage
* s3fs-fuse (https://github.com/s3fs-fuse/s3fs-fuse): FUSE-based file system backed by Amazon S3
* sshfs (https://github.com/libfuse/sshfs): A network filesystem client to connect to SSH servers
* Ceph (http://docs.ceph.com/docs/jewel/man/8/ceph-fuse/): client for Ceph distributed file system
* others, see <https://github.com/libfuse/libfuse/wiki/Filesystems>

---
**Reading/writing to GCS buckets with GCSFuse**

GCSFuse is a user-space file system for interacting with Google Cloud Storage.

```
$ gcsfuse <options> <bucket name> <mountpoint>
```

https://github.com/GoogleCloudPlatform/gcsfuse

---
**Some approaches for mounting (Fuse) filesystems**

---
**Mount Fuse filesystem directly inside container**

* more portable but adds Fuse dependencies to Docker image
* requires mounting/un-mounting the filesystem on container start/stop

![fuse inside docker diagram](images/Fuse inside docker.png)

---
**Mount Fuse filesystem on host**

* less portable; requires host support

```
docker run -v "/mnt/my-bucket:/mnt" <...>
```

![fuse mount on host](images/Fuse on host.png)

---
**Pros**

* transparent read/writes to cloud (object) network storage across cloud providers
* no need for cloud storage specific application source-code support

---
**Cons**

* fuse mount inside container requires elevated priviledges (`CAP_SYS_ADMIN` for kernel module loading)
* but possible to configure a "least privileges" security context
* fuse mount on host requires configuration management (container scheduling less portable?)
* latency is (obviously ?) higher than a regular filesystem
* eventually consistent

---
**Security considerations**

---
**Fuse mounts inside container require elevated privileges**

```
$ docker run -it --rm \
  --privileged=false \
  -e GCS_BUCKET_NAME=fuse-gcs-demo \
  -v "$(pwd)/demo-credentials.json:/etc/gcloud/service-account.json" \
  registry.gitlab.com/zemanel/gcfuse-k8-demo:latest bash

+ cmd=bash
+ echo Mounting fuse-gcs-demo bucket
Mounting fuse-gcs-demo bucket
+ gcsfuse -o allow_other fuse-gcs-demo /mnt
Using mount point: /mnt
Opening GCS connection...
Opening bucket...
Mounting file system...
daemonize.Run: readFromProcess: sub-process: mountWithArgs: mountWithConn: Mount: mount: running fusermount: exit status 1

stderr:
fusermount: fuse device not found, try 'modprobe fuse' first
```

---
**Running container with minimum elevated privileges**

* By default, Docker containers are “unprivileged”
* For mounting filesystems, `CAP_SYS_ADMIN` kernel capability is required.

Docs:

* Docker: https://docs.docker.com/engine/reference/run/#runtime-privilege-and-linux-capabilities
* kernel: http://man7.org/linux/man-pages/man7/capabilities.7.html


---
***Mounting a Fuse filesystem***
```
$ docker run -it --rm \
  --privileged=false \
  --device /dev/fuse \
  --cap-add SYS_ADMIN \
  -e GCS_BUCKET_NAME=fuse-gcs-demo \
  -v "$(pwd) /demo-credentials.json:/etc/gcloud/service-account.json" \
  registry.gitlab.com/zemanel/gcfuse-k8-demo:latest bash

+ cmd=bash
+ echo Mounting fuse-gcs-demo bucket
Mounting fuse-gcs-demo bucket
+ gcsfuse -o allow_other fuse-gcs-demo /mnt
Using mount point: /mnt
Opening GCS connection...
Opening bucket...
Mounting file system...
File system has been successfully mounted.
+ echo Executing cmd: bash
Executing cmd: bash
+ exec bash
```



---
**Docker Demo**

```
  <...>
  fuse-watcher:
    build: .
    command: ["watchmedo3", "log", "--debug-force-polling", "/mnt/dumps"]
    environment:
    - GCS_BUCKET_NAME=fuse-gcs-demo
    - PYTHONUNBUFFERED=1
    volumes:
    # Gcloud credentials file for gcloudfuse access
    - "./demo-credentials.json:/etc/gcloud/service-account.json"
    privileged: false
    devices:
    - "/dev/fuse"
    cap_add:
    - SYS_ADMIN  # required by Fuse
```

---
**Kubernetes Demo**

Kubernetes currently requires `privileged: true`, but there is hope in "Device Plugins"

See: <https://github.com/kubernetes/kubernetes/issues/5607>

```
    <...>
    containers:
    - name: fuse-watcher
    image: registry.gitlab.com/zemanel/gcfuse-k8-demo:latest
    args: ["watchmedo3", "log", "--debug-force-polling", "/mnt/dumps"]
    imagePullPolicy: Always
    securityContext:
        privileged: true  # <---- this is dangerous
    env:
        - name: GCS_BUCKET_NAME
        value: fuse-gcs-demo
        - name: PYTHONUNBUFFERED
        value: "1"
    <...>
```

---
![Hiring](images/Presentation_slide.png)
---
**Keep in touch!**

* Twitter: @zemanel
* E-mail: jose.moreira@container-solutions.com
* LinkedIn: https://www.linkedin.com/in/josemoreira

Slides & demo code

* https://gitlab.com/zemanel/goto-night-talk-fuse-docker
* https://gitlab.com/zemanel/gcfuse-k8-demo/pipelines

---
**Questions?**
